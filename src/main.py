from flask import Flask, request, jsonify
import base64
import redis
import os, sys

app = Flask(__name__)

connection = redis.Redis(
    host=os.environ["REDIS_HOST"],
    port=int(os.environ["REDIS_PORT"]))

@app.route("/", methods=["GET", "POST"])
def index():
    try:

        if request.method == "POST":

            connection.rpush(request.values["chat_id"].encode("utf-8"),  base64.b64decode(request.values["content"].encode("utf-8"), b"-_"))

            return "OK"

        else:

            messages = [base64.b64encode(m, b"-_").decode("utf-8") for m in  connection.lrange(request.values["chat_id"].encode("utf-8"), int(request.values["start"]), int(request.values["end"]))]

            return jsonify(messages)

    except Exception as e:

        print(e,file=sys.stderr)

        return "ERROR", 500

